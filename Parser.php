<?php

require_once 'ParserInterface.php';

/**
 * @author Victor Zinchenko <zinchenko.us@gmail.com>
 */
class Parser implements ParserInterface
{

    public function process(string $url, string $tag): array
    {
        return [
            'just',
            'do',
            'it'
        ];
    }

}
